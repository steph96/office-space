import { AngularMaterialModule } from './_modules/angularMaterial';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HomeComponent } from './components/home/home.component';
import { OfficeCardComponent } from './components/office-card/office-card.component';
import { OfficeViewComponent } from './components/office-view/office-view.component';
import { AddOfficeModalComponent } from './_modals/add-office-modal/add-office-modal.component';
import { RemoveOfficeModalComponent } from './_modals/remove-office-modal/remove-office-modal.component';
import { OfficeViewModule } from './components/office-view/office-view.module';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        OfficeCardComponent,
        OfficeViewComponent,
        AddOfficeModalComponent,
        RemoveOfficeModalComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        AngularMaterialModule,
        OfficeViewModule,
        FormsModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
    entryComponents: [AddOfficeModalComponent, RemoveOfficeModalComponent],
})
export class AppModule {}
