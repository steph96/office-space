import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-remove-office-modal',
    templateUrl: './remove-office-modal.component.html',
    styleUrls: ['./remove-office-modal.component.scss'],
})
export class RemoveOfficeModalComponent implements OnInit {
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<RemoveOfficeModalComponent>,
    ) {}

    ngOnInit() {}

    handleConfirmClick(): void {
        this.data.action = true;
        this.dialogRef.close(this.data);
    }

    handleCancelClick(): void {
        this.data.action = false;
        this.dialogRef.close(this.data);
    }
}
