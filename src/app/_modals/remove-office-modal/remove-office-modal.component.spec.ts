import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveOfficeModalComponent } from './remove-office-modal.component';

describe('RemoveOfficeModalComponent', () => {
  let component: RemoveOfficeModalComponent;
  let fixture: ComponentFixture<RemoveOfficeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveOfficeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveOfficeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
