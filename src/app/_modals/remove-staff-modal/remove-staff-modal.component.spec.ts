import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveStaffModalComponent } from './remove-staff-modal.component';

describe('RemoveStaffModalComponent', () => {
  let component: RemoveStaffModalComponent;
  let fixture: ComponentFixture<RemoveStaffModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveStaffModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveStaffModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
