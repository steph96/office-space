import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-remove-staff-modal',
    templateUrl: './remove-staff-modal.component.html',
    styleUrls: ['./remove-staff-modal.component.scss'],
})
export class RemoveStaffModalComponent implements OnInit {
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<RemoveStaffModalComponent>,
    ) {}

    ngOnInit() {}

    handleConfirmClick(): void {
        this.data.action = true;
        this.dialogRef.close(this.data);
    }

    handleCancelClick(): void {
        this.data.action = false;
        this.dialogRef.close(this.data);
    }
}
