import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-add-staff-modal',
    templateUrl: './add-staff-modal.component.html',
    styleUrls: ['./add-staff-modal.component.scss'],
})
export class AddStaffModalComponent implements OnInit {
    constructor(@Inject(MAT_DIALOG_DATA) public data: any, private dialogRef: MatDialogRef<AddStaffModalComponent>) {}

    ngOnInit() {}

    handleConfirmClick(): void {
        this.dialogRef.close(this.data);
    }

    handleCancelClick(): void {
        this.dialogRef.close();
    }
}
