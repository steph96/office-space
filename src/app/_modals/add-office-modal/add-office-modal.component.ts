import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {NgForm } from '@angular/forms';

@Component({
    selector: 'app-add-office-modal',
    templateUrl: './add-office-modal.component.html',
    styleUrls: ['./add-office-modal.component.scss'],
})
export class AddOfficeModalComponent implements OnInit {
    arColours: ['Red', 'Green', 'Pink', 'Yellow', 'Blue', 'Purple'];

    constructor(@Inject(MAT_DIALOG_DATA) public data: any, private dialogRef: MatDialogRef<AddOfficeModalComponent>) {}

    ngOnInit() {}

    handleConfirmClick(): void {
        this.dialogRef.close(this.data);
    }

    handleCancelClick(): void {
        this.dialogRef.close();
    }
}
