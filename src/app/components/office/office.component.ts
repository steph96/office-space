import { cloneDeep } from 'lodash';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OfficeService } from 'src/app/_services/office.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Office, Staff } from 'src/app/_models/baseModels';
import { MatDialog } from '@angular/material/dialog';
import { AddStaffModalComponent } from 'src/app/_modals/add-staff-modal/add-staff-modal.component';
import { take } from 'rxjs/operators';
import { RemoveStaffModalComponent } from 'src/app/_modals/remove-staff-modal/remove-staff-modal.component';

@Component({
    selector: 'app-office',
    templateUrl: './office.component.html',
    styleUrls: ['./office.component.scss'],
})
export class OfficeComponent implements OnInit {
    arStaff = [];
    arSubs: Subscription[] = [];

    officeId;

    arOffices = [];

    obOffice: Office;

    numOfStaff = 0;

    searchString;
    arFilteredStaff = [];

    isOpen = false;

    constructor(
        private officeService: OfficeService,
        private route: ActivatedRoute,
        private dialog: MatDialog,
        private router: Router,
    ) {}

    ngOnInit() {
        this.arStaff = [];
        this.arSubs.push(
            this.officeService.dataSourceStaff.subscribe((data) => {
                this.arStaff = cloneDeep(data);
                this.numOfStaff = this.arStaff.length;
                this.arFilteredStaff = cloneDeep(this.arStaff);
            }),
        );
        console.log('arStaff', this.arStaff);

        this.arSubs.push(
            this.route.params.subscribe((params) => {
                this.officeId = params['id'];
            }),
        );

        this.arSubs.push(
            this.officeService.dataSource.subscribe((data) => {
                this.arOffices = cloneDeep(data);
                const index = this.arOffices.findIndex((i) => i.id === this.officeId);
                this.obOffice = this.arOffices[index];
            }),
        );

        console.log('OBOFFICE', this.obOffice);
    }

    handleSearch(searchString) {
        console.log('BEFORE', this.arStaff);
        this.arFilteredStaff = this.arStaff.filter((word) => word.name.includes(searchString));
        console.log('this.arFilteredStaff', this.arFilteredStaff);
    }

    handleAddStaff() {
        const dialogRef = this.dialog.open(AddStaffModalComponent, {
            data: {
                id: '',
                name: '',
                surname: '',
                officeId: this.officeId,
                action: 'Add',
            },
        });

        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe((result) => {
                this.officeService.addStaff(result, this.obOffice.maxOccupants);
            });
    }

    handleEditStaff(obStaff: Staff) {
        const dialogRef = this.dialog.open(AddStaffModalComponent, {
            data: {
                id: obStaff.id,
                name: obStaff.name,
                surname: obStaff.surname,
                officeId: this.officeId,
                action: 'Edit',
            },
        });

        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe((result) => {
                this.officeService.editStaff(obStaff, result);
            });
    }

    handleRemoveStaff(obStaff: Staff) {
        const dialogRef = this.dialog.open(RemoveStaffModalComponent, {
            data: {
                name: obStaff.name,
                action: false,
            },
        });

        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe((result) => {
                this.officeService.deleteStaff(obStaff, result.action);
            });
    }

    handleBackToHome() {
        this.router.navigate(['/home']);
    }

    handleChangeButton() {
        this.isOpen = !this.isOpen;
    }
}
