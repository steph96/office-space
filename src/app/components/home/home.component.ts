import { cloneDeep } from 'lodash';
import { Office } from './../../_models/baseModels';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AddOfficeModalComponent } from 'src/app/_modals/add-office-modal/add-office-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { take } from 'rxjs/operators';
import { OfficeService } from 'src/app/_services/office.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { RemoveOfficeModalComponent } from 'src/app/_modals/remove-office-modal/remove-office-modal.component';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
    constructor(private dialog: MatDialog, private officeService: OfficeService, private router: Router) {}

    arSubs: Subscription[] = [];

    arOffices = [];
    arStaff = [];
    numOfStaff = 0;

    isOpen = false;

    ngOnInit() {
        this.arOffices = [];
        this.arSubs.push(
            this.officeService.dataSource.subscribe((data) => {
                console.log('DATA HOME:', data);
                this.arOffices = cloneDeep(data);
            }),
        );
        console.log('arOffices', this.arOffices);

        this.arStaff = [];
        this.arSubs.push(
            this.officeService.dataSourceStaff.subscribe((data) => {
                this.arStaff = cloneDeep(data);
                this.numOfStaff = this.arStaff.length;
            }),
        );
    }

    handleAddOffice() {
        const dialogRef = this.dialog.open(AddOfficeModalComponent, {
            data: {
                name: '',
                email: '',
                tel: '',
                address: '',
                maxOccupants: '',
                color: '',
                id: '',
                action: 'Add',
            },
        });

        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe((result) => {
                console.log('RESULT:', result);
                this.officeService.addOffice(result);
            });
    }

    handleRemoveOffice(obOffice: Office) {
        const dialogRef = this.dialog.open(RemoveOfficeModalComponent, {
            data: {
                name: obOffice.name,
                action: false,
            },
        });

        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe((result) => {
                this.officeService.deleteOffice(obOffice, result.action);
            });
    }

    handleEditOffice(obOffice: Office) {
        const dialogRef = this.dialog.open(AddOfficeModalComponent, {
            data: {
                name: obOffice.name,
                email: obOffice.email,
                tel: obOffice.tel,
                address: obOffice.address,
                maxOccupants: obOffice.maxOccupants,
                color: '',
                id: obOffice.id,
                action: 'Edit',
            },
        });

        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe((result) => {
                this.officeService.editOffice(obOffice, result);
                console.log('RESULT EDIT:', result);
            });
    }

    handleOfficeClick(obOffice) {
        this.router.navigate([`office/${obOffice.id}`]);
    }

    handleChangeButton() {
        this.isOpen = !this.isOpen;
    }

    ngOnDestroy(): void {
        this.arSubs.forEach((sub) => sub.unsubscribe());
    }
}
