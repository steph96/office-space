import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfficeComponent } from '../office/office.component';


const routes: Routes = [
    {
        path: 'office/:id',
        component: OfficeComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class OfficeViewRoutingModule {}
