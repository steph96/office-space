import { OfficeViewComponent } from './office-view.component';
import { AngularMaterialModule } from './../../_modules/angularMaterial';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OfficeViewRoutingModule } from './office-view-routing.module';
import { AddStaffModalComponent } from '../../_modals/add-staff-modal/add-staff-modal.component';
import { RemoveStaffModalComponent } from '../../_modals/remove-staff-modal/remove-staff-modal.component';
import { FormsModule } from '@angular/forms';
import { OfficeComponent } from '../office/office.component';

@NgModule({
    declarations: [AddStaffModalComponent, RemoveStaffModalComponent, OfficeComponent],
    imports: [CommonModule, OfficeViewRoutingModule, FormsModule, AngularMaterialModule],
    entryComponents: [AddStaffModalComponent, RemoveStaffModalComponent],
})
export class OfficeViewModule {}
