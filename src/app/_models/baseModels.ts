export interface Office {
    id: string;
    name: string;
    address: string;
    email: string;
    tel?: string;
    maxOccupants: number;
    color?: string;
    currentOccupants?: number;
}

export interface Staff {
    id: string;
    officeId: string;
    name: string;
    surname: string;
}

// Id: string(required) randomly generated alphanumeric 24 characters
// ● Name: string (required)
// ● Location: string(required)
// ● Email: string(required)
// ● Tell Number: string(optional)
// ● Max number of occupants: number(required)
// ● Color: string (optional) if no color is chosen t
