import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Office, Staff } from '../_models/baseModels';
import { cloneDeep } from 'lodash';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class OfficeService {
    public dataSource = new BehaviorSubject<Office[]>([]);
    public dataSourceStaff = new BehaviorSubject<Staff[]>([]);

    constructor() {}

    oldOffices = [];
    oldStaff = [];

    // #region office
    addOffice(data: Office) {
        this.oldOffices = cloneDeep(this.dataSource.value);
        this.oldOffices.push(data);
        this.dataSource.next(this.oldOffices);
    }

    editOffice(oldOffice: Office, newOffice: Office) {
        this.oldOffices = cloneDeep(this.dataSource.value);
        const index = this.oldOffices.findIndex((i) => i.id === oldOffice.id);
        this.oldOffices.splice(index, 1, newOffice);
        this.dataSource.next(this.oldOffices);
    }

    deleteOffice(data: Office, action: boolean) {
        if (action === true) {
            this.oldOffices = cloneDeep(this.dataSource.value);
            const index = this.oldOffices.findIndex((i) => i.id === data.id);
            this.oldOffices.splice(index, 1);
            this.dataSource.next(this.oldOffices);
        }
    }
    // #endregion

    // #region Staff
    addStaff(obStaff: Staff, limit: number) {
        this.oldStaff = cloneDeep(this.dataSourceStaff.value);
        console.log('Length1', this.oldStaff.length);
        console.log('Length2', limit);

        if (this.oldStaff.length < limit) {
            this.oldStaff.push(obStaff);
            this.dataSourceStaff.next(this.oldStaff);
        } else {
            console.log('There is no more space in the office');
        }
    }

    editStaff(oldStaff: Staff, newStaff: Staff) {
        this.oldStaff = cloneDeep(this.dataSourceStaff.value);
        const index = this.oldStaff.findIndex((i) => i.id === oldStaff.id);
        this.oldStaff.splice(index, 1, newStaff);
        this.dataSourceStaff.next(this.oldStaff);
    }

    deleteStaff(data: Staff, action: boolean) {
        if (action === true) {
            this.oldStaff = cloneDeep(this.dataSourceStaff.value);
            const index = this.oldStaff.findIndex((i) => i.id === data.id);
            console.log('oldStaff1', this.oldStaff);
            this.oldStaff.splice(index, 1);
            console.log('oldStaff2', this.oldStaff);
            this.dataSourceStaff.next(this.oldStaff);
        }
    }

    // #endregion
}
