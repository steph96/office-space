import { MatIconModule } from '@angular/material/icon';
import { NgModule } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
    declarations: [],
    imports: [MatIconModule, MatInputModule, FormsModule, MatDialogModule, MatMenuModule, MatSelectModule],
    exports: [MatIconModule, MatInputModule, FormsModule, MatDialogModule, MatMenuModule, MatSelectModule],
})
export class AngularMaterialModule {}
