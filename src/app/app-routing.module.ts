import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { OfficeViewComponent } from './components/office-view/office-view.component';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    {
        path: 'home',
        component: HomeComponent,
    },

    {
        path: 'office',
        component: OfficeViewComponent,
        loadChildren: () => import('./components/office-view/office-view.module').then((m) => m.OfficeViewModule),
    },
    // {
    //     path: 'office/:id',
    //     component: OfficeViewComponent,
    // },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
